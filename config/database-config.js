module.exports = {
	development: {
		username: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		database: process.env.DB_NAME,
		host: process.env.DB_HOSTNAME,
		port: process.env.DB_PORT,
		dialect: 'postgres',
	},
	test: {
		username: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		database: process.env.DB_NAME,
		host: process.env.DB_HOSTNAME,
		port: process.env.DB_PORT,
		dialect: 'postgres',
	},
	production: {
		username: process.env.CLOUD_DB_USERNAME,
		password: process.env.CLOUD_DB_PASSWORD,
		database: process.env.CLOUD_DB_NAME,
		host: `/cloudsql/${process.env.CLOUD_DB_INSTANCE_CONNECTION_NAME}`,
		port: process.env.CLOUD_DB_PORT,
		dialect: 'postgres',
	},
};
